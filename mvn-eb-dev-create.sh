#!/bin/bash

# how to use
# ./mvn-eb-create.sh ${PROFILE}
# Default profile is 'dev', and 'apne1' and 'apse1' can be used for other production environments.

PROFILE=dev

if [[ $# > 0 ]]; then
  PROFILE=$1
fi

mvn -P$PROFILE clean package 

mvn -P$PROFILE beanstalk:upload-source-bundle beanstalk:create-application-version beanstalk:create-environment


