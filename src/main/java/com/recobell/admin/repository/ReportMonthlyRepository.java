package com.recobell.admin.repository;

import com.recobell.admin.domain.ReportMonthly;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * Spring Data JPA repository for the ReportMonthly entity.
 */
public interface ReportMonthlyRepository extends JpaRepository<ReportMonthly,Long> {

	List<ReportMonthly> findAllByCuidAndChannelAndDateBetweenOrderByDateAsc(UUID cuid, String channel, LocalDate sdate, LocalDate edate);
	
	@Override
	<S extends ReportMonthly> List<S> save(Iterable<S> entities);
	
	@Override
	void delete(ReportMonthly r);
}
