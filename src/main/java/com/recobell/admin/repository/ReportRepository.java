package com.recobell.admin.repository;

import java.time.LocalDate;
import java.util.UUID;

public abstract class ReportRepository{
	
    protected UUID stringToUUID(String cuid){
    	return UUID.fromString(cuid);
    }
    
    protected LocalDate stringToLocalDate(String date){
    	if(date.equals("now"))
    		return LocalDate.now();
		else
			return LocalDate.parse(date);
    }
    
}