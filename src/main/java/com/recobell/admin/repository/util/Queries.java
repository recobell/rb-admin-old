package com.recobell.admin.repository.util;

public class Queries {
	public static final String FIND_MANAGED_CUSTOMERS = 
			"SELECT b " + 
			"FROM " + 
				"User a " + 
				"LEFT OUTER JOIN a.customers b " +
			"WHERE a.username = :username";
}

