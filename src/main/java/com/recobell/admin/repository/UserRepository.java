package com.recobell.admin.repository;

import com.recobell.admin.domain.User;
import com.recobell.admin.repository.util.Queries;

import java.time.ZonedDateTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByUsername(String username);

    Optional<User> findOneById(Long id);

    @Query(Queries.FIND_MANAGED_CUSTOMERS)
    List<User> findManagedCustomersByUsername(@Param("username") String username);
    
    @Override
    void delete(User t);

}
