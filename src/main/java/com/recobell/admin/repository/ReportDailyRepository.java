package com.recobell.admin.repository;

import com.recobell.admin.domain.ReportDaily;

import org.springframework.data.jpa.repository.*;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * Spring Data JPA repository for the ReportDaily entity.
 */
public interface ReportDailyRepository extends JpaRepository<ReportDaily,Long> {
	
	List<ReportDaily> findAllByCuidAndChannelAndDateBetweenOrderByDateAsc(UUID cuid, String channel, LocalDate sdate, LocalDate edate);
	
	@Override
	<S extends ReportDaily> List<S> save(Iterable<S> entities);
	
	@Override
	void delete(ReportDaily r);
}
