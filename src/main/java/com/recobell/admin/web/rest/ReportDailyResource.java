package com.recobell.admin.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.recobell.admin.domain.ReportDaily;
import com.recobell.admin.repository.ReportDailyRepository;
import com.recobell.admin.repository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * REST controller for managing ReportDaily.
 */
@RestController
@RequestMapping("/api")
public class ReportDailyResource extends ReportRepository{

    private final Logger log = LoggerFactory.getLogger(ReportDailyResource.class);

    @Inject
    private ReportDailyRepository reportDailyRepository;

    @RequestMapping(value = "/dailyReports/{cuid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ReportDaily> getReportsDaily(
    	@PathVariable String cuid,
    	@RequestParam(value = "channel", required = true) String channel,
    	@RequestParam(value = "sdate", required = true) String sdate,
    	@RequestParam(value = "edate", required = true) String edate
    ) {
    	log.debug("REST request to get DailyReport : {}, {}, {} ~ {}", cuid, channel, sdate, edate);
    	return reportDailyRepository.findAllByCuidAndChannelAndDateBetweenOrderByDateAsc(stringToUUID(cuid), channel, stringToLocalDate(sdate), stringToLocalDate(edate));
    }

}
