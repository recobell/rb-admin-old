package com.recobell.admin.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.recobell.admin.domain.ReportMonthly;
import com.recobell.admin.repository.ReportMonthlyRepository;
import com.recobell.admin.repository.ReportRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

/**
 * REST controller for managing ReportMonthly.
 */
@RestController
@RequestMapping("/api")
public class ReportMonthlyResource extends ReportRepository{

    private final Logger log = LoggerFactory.getLogger(ReportMonthlyResource.class);

    @Inject
    private ReportMonthlyRepository reportMonthlyRepository;
    
    @RequestMapping(value = "/monthlyReports/{cuid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ReportMonthly> getReportsMonthly(
    		@PathVariable String cuid,
    		@RequestParam(value = "channel", required = true) String channel,
        	@RequestParam(value = "sdate", required = true) String sdate,
        	@RequestParam(value = "edate", required = true) String edate
    ) {
    	log.debug("REST request to get MonthlyReports : {}, {}, {} ~ {}", cuid, channel, sdate, edate);
    	return reportMonthlyRepository.findAllByCuidAndChannelAndDateBetweenOrderByDateAsc(stringToUUID(cuid), channel, stringToLocalDate(sdate), stringToLocalDate(edate)); 
    }
    
}
