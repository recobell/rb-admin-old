/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.recobell.admin.web.rest.dto;
