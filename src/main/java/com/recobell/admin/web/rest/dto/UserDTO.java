package com.recobell.admin.web.rest.dto;

import com.recobell.admin.domain.Authority;
import com.recobell.admin.domain.User;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.*;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    public static final int PASSWORD_MIN_LENGTH = 5;
    public static final int PASSWORD_MAX_LENGTH = 100;

    @Pattern(regexp = "^[a-z0-9]*$")
    @NotNull
    @Size(min = 1, max = 50)
    private String username;

    @NotNull
    @Size(min = PASSWORD_MIN_LENGTH, max = PASSWORD_MAX_LENGTH)
    private String password;

    @Size(max = 100)
    private String name;
    
    @Email
    @Size(min = 5, max = 100)
    private String email;

    private boolean activated = false;

    @Size(min = 2, max = 5)
    private String langKey;
    
    @Size(max = 20)
    private String currency;

    private Set<String> authorities;
    
    private UUID cuid;
    
    private String customerName;


	private String customerURL;
    private String phoneNum;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(user.getUsername(), null, user.getName(),
            user.getEmail(), user.getActivated(), user.getLangKey(), user.getCurrency(),
            user.getAuthorities().stream().map(Authority::getName)
                .collect(Collectors.toSet()), user.getCuid(),
            user.getCustomerName(), user.getCustomerURL(), user.getPhoneNum());
    }

    public UserDTO(String login, String password, String name,
        String email, boolean activated, String langKey, String currency, Set<String> authorities, UUID cuid,
        String customerName, String customerURL, String phoneNum) {

        this.username = login;
        this.password = password;
        this.name = name;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.currency = currency;
        this.authorities = authorities;
        this.cuid = cuid;
        this.customerName = customerName;
        this.customerURL = customerURL;
        this.phoneNum = phoneNum;
    }

    public UUID getCuid() {
		return cuid;
	}

	public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
    
    public String getName() {
    	return name;
    }
    
    public String getEmail() {
        return email;
    }

    public boolean isActivated() {
        return activated;
    }

    public String getLangKey() {
        return langKey;
    }

    public String getCurrency() {
		return currency;
	}

	public Set<String> getAuthorities() {
        return authorities;
    }
    
    public String getCustomerName() {
		return customerName;
	}


	public String getCustomerURL() {
		return customerURL;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

    @Override
    public String toString() {
        return "UserDTO{" +
            "username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", activated=" + activated +
            ", langKey='" + langKey + '\'' +
            ", currency='" + currency + '\'' +
            ", authorities=" + authorities + '\'' +
            ", cuid=" + cuid + 
            "}";
    }
}
