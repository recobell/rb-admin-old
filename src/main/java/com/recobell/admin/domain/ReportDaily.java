package com.recobell.admin.domain;

import java.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.hibernate.annotations.Type;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A ReportDaily.
 */
@Entity
@Table(name = "report_daily")
public class ReportDaily implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Type(type="pg-uuid")
    @Column(name = "cuid", nullable = false)
    private UUID cuid;

    @NotNull
    @Column(name = "date", nullable = false)
    private LocalDate date;

    @NotNull
    @Size(max = 10)
    @Column(name = "channel", length = 10, nullable = false)
    private String channel;

    @Column(name = "session_visit_total")
    private Integer sessionVisitTotal;

    @Column(name = "session_visit_reco")
    private Integer sessionVisitReco;

    @Column(name = "session_visit_rate")
    private Double sessionVisitRate;

    @Column(name = "unique_visit_total")
    private Integer uniqueVisitTotal;

    @Column(name = "unique_visit_reco")
    private Integer uniqueVisitReco;

    @Column(name = "unique_visit_rate")
    private Double uniqueVisitRate;

    @Column(name = "page_view_total")
    private Integer pageViewTotal;

    @Column(name = "page_view_reco")
    private Integer pageViewReco;

    @Column(name = "page_view_rate")
    private Double pageViewRate;

    @Column(name = "purchase_count_total")
    private Integer purchaseCountTotal;

    @Column(name = "purchase_count_reco")
    private Integer purchaseCountReco;

    @Column(name = "purchase_count_rate")
    private Double purchaseCountRate;

    @Column(name = "revenue_total")
    private Double revenueTotal;

    @Column(name = "revenue_reco")
    private Double revenueReco;

    @Column(name = "revenue_rate")
    private Double revenueRate;

    @Column(name = "customer_transaction_avg_total")
    private Double customerTransactionAvgTotal;

    @Column(name = "customer_transaction_avg_reco")
    private Double customerTransactionAvgReco;

    @Column(name = "customer_transaction_avg_result")
    private Double customerTransactionAvgResult;

    @Column(name = "purchase_item_count_avg_total")
    private Double purchaseItemCountAvgTotal;

    @Column(name = "purchase_item_count_avg_reco")
    private Double purchaseItemCountAvgReco;

    @Column(name = "purchase_item_count_avg_result")
    private Double purchaseItemCountAvgResult;

    @Column(name = "visit_item_click_avg_total")
    private Double visitItemClickAvgTotal;

    @Column(name = "visit_item_click_avg_reco")
    private Double visitItemClickAvgReco;

    @Column(name = "visit_item_click_avg_result")
    private Double visitItemClickAvgResult;

    @Column(name = "purchase_conversion_rate_total")
    private Double purchaseConversionRateTotal;

    @Column(name = "purchase_conversion_rate_reco")
    private Double purchaseConversionRateReco;

    @Column(name = "purchase_conversion_rate_result")
    private Double purchaseConversionRateResult;

    @Column(name = "session_purchase_revenue_avg_total")
    private Double sessionPurchaseRevenueAvgTotal;

    @Column(name = "session_purchase_revenue_avg_reco")
    private Double sessionPurchaseRevenueAvgReco;

    @Column(name = "session_purchase_revenue_avg_result")
    private Double sessionPurchaseRevenueAvgResult;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UUID getCuid() {
        return cuid;
    }

    public void setCuid(UUID cuid) {
        this.cuid = cuid;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getSessionVisitTotal() {
        return sessionVisitTotal;
    }

    public void setSessionVisitTotal(Integer sessionVisitTotal) {
        this.sessionVisitTotal = sessionVisitTotal;
    }

    public Integer getSessionVisitReco() {
        return sessionVisitReco;
    }

    public void setSessionVisitReco(Integer sessionVisitReco) {
        this.sessionVisitReco = sessionVisitReco;
    }

    public Double getSessionVisitRate() {
        return sessionVisitRate;
    }

    public void setSessionVisitRate(Double sessionVisitRate) {
        this.sessionVisitRate = sessionVisitRate;
    }

    public Integer getUniqueVisitTotal() {
        return uniqueVisitTotal;
    }

    public void setUniqueVisitTotal(Integer uniqueVisitTotal) {
        this.uniqueVisitTotal = uniqueVisitTotal;
    }

    public Integer getUniqueVisitReco() {
        return uniqueVisitReco;
    }

    public void setUniqueVisitReco(Integer uniqueVisitReco) {
        this.uniqueVisitReco = uniqueVisitReco;
    }

    public Double getUniqueVisitRate() {
        return uniqueVisitRate;
    }

    public void setUniqueVisitRate(Double uniqueVisitRate) {
        this.uniqueVisitRate = uniqueVisitRate;
    }

    public Integer getPageViewTotal() {
        return pageViewTotal;
    }

    public void setPageViewTotal(Integer pageViewTotal) {
        this.pageViewTotal = pageViewTotal;
    }

    public Integer getPageViewReco() {
        return pageViewReco;
    }

    public void setPageViewReco(Integer pageViewReco) {
        this.pageViewReco = pageViewReco;
    }

    public Double getPageViewRate() {
        return pageViewRate;
    }

    public void setPageViewRate(Double pageViewRate) {
        this.pageViewRate = pageViewRate;
    }

    public Integer getPurchaseCountTotal() {
        return purchaseCountTotal;
    }

    public void setPurchaseCountTotal(Integer purchaseCountTotal) {
        this.purchaseCountTotal = purchaseCountTotal;
    }

    public Integer getPurchaseCountReco() {
        return purchaseCountReco;
    }

    public void setPurchaseCountReco(Integer purchaseCountReco) {
        this.purchaseCountReco = purchaseCountReco;
    }

    public Double getPurchaseCountRate() {
        return purchaseCountRate;
    }

    public void setPurchaseCountRate(Double purchaseCountRate) {
        this.purchaseCountRate = purchaseCountRate;
    }

    public Double getRevenueTotal() {
        return revenueTotal;
    }

    public void setRevenueTotal(Double revenueTotal) {
        this.revenueTotal = revenueTotal;
    }

    public Double getRevenueReco() {
        return revenueReco;
    }

    public void setRevenueReco(Double revenueReco) {
        this.revenueReco = revenueReco;
    }

    public Double getRevenueRate() {
        return revenueRate;
    }

    public void setRevenueRate(Double revenueRate) {
        this.revenueRate = revenueRate;
    }

    public Double getCustomerTransactionAvgTotal() {
        return customerTransactionAvgTotal;
    }

    public void setCustomerTransactionAvgTotal(Double customerTransactionAvgTotal) {
        this.customerTransactionAvgTotal = customerTransactionAvgTotal;
    }

    public Double getCustomerTransactionAvgReco() {
        return customerTransactionAvgReco;
    }

    public void setCustomerTransactionAvgReco(Double customerTransactionAvgReco) {
        this.customerTransactionAvgReco = customerTransactionAvgReco;
    }

    public Double getCustomerTransactionAvgResult() {
        return customerTransactionAvgResult;
    }

    public void setCustomerTransactionAvgResult(Double customerTransactionAvgResult) {
        this.customerTransactionAvgResult = customerTransactionAvgResult;
    }

    public Double getPurchaseItemCountAvgTotal() {
        return purchaseItemCountAvgTotal;
    }

    public void setPurchaseItemCountAvgTotal(Double purchaseItemCountAvgTotal) {
        this.purchaseItemCountAvgTotal = purchaseItemCountAvgTotal;
    }

    public Double getPurchaseItemCountAvgReco() {
        return purchaseItemCountAvgReco;
    }

    public void setPurchaseItemCountAvgReco(Double purchaseItemCountAvgReco) {
        this.purchaseItemCountAvgReco = purchaseItemCountAvgReco;
    }

    public Double getPurchaseItemCountAvgResult() {
        return purchaseItemCountAvgResult;
    }

    public void setPurchaseItemCountAvgResult(Double purchaseItemCountAvgResult) {
        this.purchaseItemCountAvgResult = purchaseItemCountAvgResult;
    }

    public Double getVisitItemClickAvgTotal() {
        return visitItemClickAvgTotal;
    }

    public void setVisitItemClickAvgTotal(Double visitItemClickAvgTotal) {
        this.visitItemClickAvgTotal = visitItemClickAvgTotal;
    }

    public Double getVisitItemClickAvgReco() {
        return visitItemClickAvgReco;
    }

    public void setVisitItemClickAvgReco(Double visitItemClickAvgReco) {
        this.visitItemClickAvgReco = visitItemClickAvgReco;
    }

    public Double getVisitItemClickAvgResult() {
        return visitItemClickAvgResult;
    }

    public void setVisitItemClickAvgResult(Double visitItemClickAvgResult) {
        this.visitItemClickAvgResult = visitItemClickAvgResult;
    }

    public Double getPurchaseConversionRateTotal() {
        return purchaseConversionRateTotal;
    }

    public void setPurchaseConversionRateTotal(Double purchaseConversionRateTotal) {
        this.purchaseConversionRateTotal = purchaseConversionRateTotal;
    }

    public Double getPurchaseConversionRateReco() {
        return purchaseConversionRateReco;
    }

    public void setPurchaseConversionRateReco(Double purchaseConversionRateReco) {
        this.purchaseConversionRateReco = purchaseConversionRateReco;
    }

    public Double getPurchaseConversionRateResult() {
        return purchaseConversionRateResult;
    }

    public void setPurchaseConversionRateResult(Double purchaseConversionRateResult) {
        this.purchaseConversionRateResult = purchaseConversionRateResult;
    }

    public Double getSessionPurchaseRevenueAvgTotal() {
        return sessionPurchaseRevenueAvgTotal;
    }

    public void setSessionPurchaseRevenueAvgTotal(Double sessionPurchaseRevenueAvgTotal) {
        this.sessionPurchaseRevenueAvgTotal = sessionPurchaseRevenueAvgTotal;
    }

    public Double getSessionPurchaseRevenueAvgReco() {
        return sessionPurchaseRevenueAvgReco;
    }

    public void setSessionPurchaseRevenueAvgReco(Double sessionPurchaseRevenueAvgReco) {
        this.sessionPurchaseRevenueAvgReco = sessionPurchaseRevenueAvgReco;
    }

    public Double getSessionPurchaseRevenueAvgResult() {
        return sessionPurchaseRevenueAvgResult;
    }

    public void setSessionPurchaseRevenueAvgResult(Double sessionPurchaseRevenueAvgResult) {
        this.sessionPurchaseRevenueAvgResult = sessionPurchaseRevenueAvgResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportDaily reportDaily = (ReportDaily) o;

        if ( ! Objects.equals(id, reportDaily.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReportDaily{" +
            "id=" + id +
            ", cuid='" + cuid + "'" +
            ", date='" + date + "'" +
            ", channel='" + channel + "'" +
            ", sessionVisitTotal='" + sessionVisitTotal + "'" +
            ", sessionVisitReco='" + sessionVisitReco + "'" +
            ", sessionVisitRate='" + sessionVisitRate + "'" +
            ", uniqueVisitTotal='" + uniqueVisitTotal + "'" +
            ", uniqueVisitReco='" + uniqueVisitReco + "'" +
            ", uniqueVisitRate='" + uniqueVisitRate + "'" +
            ", pageViewTotal='" + pageViewTotal + "'" +
            ", pageViewReco='" + pageViewReco + "'" +
            ", pageViewRate='" + pageViewRate + "'" +
            ", purchaseCountTotal='" + purchaseCountTotal + "'" +
            ", purchaseCountReco='" + purchaseCountReco + "'" +
            ", purchaseCountRate='" + purchaseCountRate + "'" +
            ", revenueTotal='" + revenueTotal + "'" +
            ", revenueReco='" + revenueReco + "'" +
            ", revenueRate='" + revenueRate + "'" +
            ", customerTransactionAvgTotal='" + customerTransactionAvgTotal + "'" +
            ", customerTransactionAvgReco='" + customerTransactionAvgReco + "'" +
            ", customerTransactionAvgResult='" + customerTransactionAvgResult + "'" +
            ", purchaseItemCountAvgTotal='" + purchaseItemCountAvgTotal + "'" +
            ", purchaseItemCountAvgReco='" + purchaseItemCountAvgReco + "'" +
            ", purchaseItemCountAvgResult='" + purchaseItemCountAvgResult + "'" +
            ", visitItemClickAvgTotal='" + visitItemClickAvgTotal + "'" +
            ", visitItemClickAvgReco='" + visitItemClickAvgReco + "'" +
            ", visitItemClickAvgResult='" + visitItemClickAvgResult + "'" +
            ", purchaseConversionRateTotal='" + purchaseConversionRateTotal + "'" +
            ", purchaseConversionRateReco='" + purchaseConversionRateReco + "'" +
            ", purchaseConversionRateResult='" + purchaseConversionRateResult + "'" +
            ", sessionPurchaseRevenueAvgTotal='" + sessionPurchaseRevenueAvgTotal + "'" +
            ", sessionPurchaseRevenueAvgReco='" + sessionPurchaseRevenueAvgReco + "'" +
            ", sessionPurchaseRevenueAvgResult='" + sessionPurchaseRevenueAvgResult + "'" +
            '}';
    }
}
