var ReportOptions = function() {
	
    var selectChannel = function () {
    	$('#channel').on('click','.btn-group .btn',function(){
    		var tmp = $(this).context;
			setChannel(tmp.name);
			makeReport();
		});
    }
    
    var selectPeriod = function () {
    	$('#period').on('click','.btn-group .btn', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'day':
	    			setPeriod('dailyReports');
	    			changeDatePickerDisplay('dailyReports');
	    			initDayDate(); break;
	    		case 'month':
	    			setPeriod('monthlyReports');
	    			changeDatePickerDisplay('monthlyReports');
	    			initMonthDate(); break;
	    		default:
    		}
    		makeReport();
    	});
    }
    
    var selectDayCalendar = function() {
    	$('#date').on('changeDate','.form-control', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'to' :
	    			setToDate(tmp.value); break;
	    		case 'from' :
	    			setFromDate(tmp.value);	break;
	    		default:
    		}
    		makeReport();
    	});
    }
    
    var selectMonthCalendar = function() {
    	$('#month').on('changeDate','.form-control', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'to' :
	    			setToDate(tmp.value); break;
	    		case 'from' :
	    			setFromDate(tmp.value);	break;
	    		default:
    		}
    		makeReport();
    	});
    }
    
    function initCuid() {
    	ChartsAmcharts.setCuid(sessionStorage.rbCuid);
    	TableDatatables.setCuid(sessionStorage.rbCuid);
		console.log(sessionStorage.rbCurrency);
    }
    
    function initDate() {
		var tmp = new Date();
		var sdate = dateToString(new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate()-14));
		var edate = dateToString(new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate()));
		$('#date [name=from]').attr('value', sdate);
		$('#date [name=to]').attr('value', edate);
		setFromDate(sdate);
		setToDate(edate);
	}
    
    function initDayDate() {
		var tmp = new Date();
		var sdate = new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate()-14);
		var edate = new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate());
		$('#date [name=from]').datepicker('setDate',sdate);
		$('#date [name=to]').datepicker('setDate',edate);
		setFromDate(dateToString(sdate));
		setToDate(dateToString(edate));
	}

	function initMonthDate() {
		var tmp = new Date();
		var sdate = new Date(tmp.getFullYear(), tmp.getMonth()-14, 1);
		var edate = new Date(tmp.getFullYear(), tmp.getMonth(), 1);
		$('#month [name=from]').datepicker('setDate',sdate);
		$('#month [name=to]').datepicker('setDate',edate);
		setFromDate(dateToString(sdate));
		setToDate(dateToString(edate));
	}
	
	function initCurrency(){
		ChartsAmcharts.setCurrency(sessionStorage.rbCurrency);
    	TableDatatables.setCurrency(sessionStorage.rbCurrency);
	}
    

    function setChannel(channel){
    	ChartsAmcharts.setChannel(channel);
    	TableDatatables.setChannel(channel);
    }
    
    function setPeriod(period){
    	ChartsAmcharts.setPeriod(period);
    	TableDatatables.setPeriod(period);
    }
    
    function setToDate(toDate){
    	ChartsAmcharts.setToDate(toDate);
    	TableDatatables.setToDate(toDate);
    }
    
    function setFromDate(fromDate){
    	ChartsAmcharts.setFromDate(fromDate);
    	TableDatatables.setFromDate(fromDate);
    }
    
    function changeDatePickerDisplay(period){
    	switch(period){
	    	case 'dailyReports':
	    		$('#date').css('display','block');
    			$('#month').css('display','none');
	    		break;
	    	case 'monthlyReports':
	    		$('#date').css('display','none');
    			$('#month').css('display','block');
	    		break;
	    	default:
    	}
    }
    
    function dateToString(date) {
    	var year = date.getFullYear();
    	var month = date.getMonth()+1;
    	var day = date.getDate();
		if(month<=9||day<=9){
			if(month<=9 && day<=9){
				return year+'-0'+month+'-0'+day;
			}else if(month<=9){
				return year+'-0'+month+'-'+day;
			}else if(day<=9){
				return year+'-'+month+'-0'+day;
			}
		}
		return year+'-'+month+'-'+day;
	}
    
    function makeReport(){
		ChartsAmcharts.makeChart();
		TableDatatables.makeTable();
    }
    
    return {
        //main function to initiate the module
        init: function () {
        	initCuid();
        	initDate();
        	initCurrency();
        	selectChannel();
        	selectPeriod();
        	selectDayCalendar();
        	selectMonthCalendar();
        },
    };

}();

jQuery(document).ready(function() {
	ReportOptions.init();
});