var TableDatatables = function () {
	
	var cuid;
	var channel;
	var edate;
	var sdate;
	var period;
	var currency;
	
	var initChannel = function(){
		channel = "AL";
	}
	
	var initPeriod = function(){
		period = "dailyReports";
	}
	
	var initDayDate = function() {
		var tmp = new Date();		
		// javascript's month 0 ~ 11
		tmp = new Date(tmp.setMonth(tmp.getMonth()+1));
		edate = dateToString(tmp);
		sdate = dateToString(new Date(tmp.setDate(tmp.getDate()-14)));
	}
	
	var initMonthDate = function() {
		var tmp = new Date();		
		// javascript's month 0 ~ 11
		tmp = new Date(new Date(tmp.setMonth(tmp.getMonth()+1)).setDate(1));
		edate = dateToString(tmp);
		sdate = dateToString(tmp);
	}

    var makeTable = function () {
    	console.log("/api/"+period+"/"+cuid+"?" +
				"channel=" + channel + 
				"&sdate=" + sdate +
				"&edate=" + edate);
        var table = $('#tablediv');
        
        var oTable = table.dataTable({
            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered1 from _MAX_ total entries)",
                "lengthMenu": "_MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},


            buttons: [
                { extend: 'print', className: 'btn dark btn-outline' },
                { extend: 'copy', className: 'btn red btn-outline' },
                { extend: 'pdf', className: 'btn green btn-outline' },
                { extend: 'excel', className: 'btn yellow btn-outline ' },
                { extend: 'csv', className: 'btn purple btn-outline ' },
                { extend: 'colvis', className: 'btn dark btn-outline', text: 'Columns'}
            ],

            // setup responsive extension: http://datatables.net/extensions/responsive/
            responsive: true,

            //"ordering": false, disable column ordering 
            //"paging": false, disable pagination
            "destroy" : true,
            "order": [
                [0, 'asc']
            ],
            
            "lengthMenu": [
                [5, 10, 15, 20, -1],
                [5, 10, 15, 20, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,
            "bAutoWidth": false,

            "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
            "ajax": {
            	"url": "/api/"+period+"/"+cuid+"?" +
				"channel=" + channel + 
				"&sdate=" + sdate +
				"&edate=" + edate,
                "dataSrc": ""
        	},
        	"columns": [
                { "data": "date", "sWidth" : "25%" },
                { "data": "pageViewTotal", "sWidth" : "25%", "mRender": function( data, type, full ) { return formatNumber(data); } },
                { "data": "pageViewReco", "sWidth" : "25%", "mRender": function( data, type, full ) { return formatNumber(data); } },
                { "data": "pageViewRate", "sWidth" : "25%", "mRender": function( data, type, full ) { return data + '%'; } }
            ]
        });
    }
    
    function formatNumber(n) {
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function dateToString(date) {
		if(date.getMonth()<=9||date.getDate()<=9){
			if(date.getMonth()<=9 && date.getDate()<=9){
				return date.getFullYear()+'-0'+date.getMonth()+'-0'+date.getDate();
			}else if(date.getMonth()<=9){
				return date.getFullYear()+'-0'+date.getMonth()+'-'+date.getDate();
			}else if(date.getDate()<=9){
				return date.getFullYear()+'-'+date.getMonth()+'-0'+date.getDate();
			}
		}
		return date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate();
	}
    
    function setCuid(newCuid) {
    	cuid = newCuid;
    }
	
	function setChannel(newChannel) {
		channel = newChannel;
	}
	
	function setPeriod(newPeriod) {
		period = newPeriod;
	}
	
	function setToDate(newToDate) {
		edate = newToDate;
	}
	
	function setFromDate(newFromDate) {
		sdate = newFromDate;
	}
	
	function setCurrency(newCurrency){
		currency = newCurrency;
	}

    return {

        //main function to initiate the module
        init: function () {
        	initChannel();
			initPeriod();
			initDayDate();
        },
		makeTable: function() {
			makeTable();
		},
		setCuid: function(newCuid) {
			setCuid(newCuid);
		},
		setChannel: function(newChannel) {
			setChannel(newChannel);
		},
		setPeriod: function(newPeriod) {
			setPeriod(newPeriod);
		},
		setToDate: function(newToDate) {
			setToDate(newToDate);
		},
		setFromDate: function(newFromDate) {
			setFromDate(newFromDate);
		},
		setDayDate: function(){
			initDayDate();
		},
		setMonthDate: function(){
			initMonthDate();
		},
		setCurrency: function(newCurrency){
			setCurrency(newCurrency);
		}
    };

}();

jQuery(document).ready(function() {
	TableDatatables.init();
});