var ChartsAmcharts = function() {
	
	var cuid;
	var channel;
	var edate;
	var sdate;
	var period;
	var currency;
	
	var initChannel = function(){
		channel = "AL";
	}
	
	var initPeriod = function(){
		period = "dailyReports";
	}
	
	var initDayDate = function() {
		var tmp = new Date();		
		// javascript's month 0 ~ 11
		tmp = new Date(tmp.setMonth(tmp.getMonth()+1));
		edate = dateToString(tmp);
		sdate = dateToString(new Date(tmp.setDate(tmp.getDate()-14)));
	}
	
	var initMonthDate = function() {
		var tmp = new Date();		
		// javascript's month 0 ~ 11
		tmp = new Date(new Date(tmp.setMonth(tmp.getMonth()+1)).setDate(1));
		edate = dateToString(tmp);
		sdate = dateToString(tmp);
	}
	
	var makeChart = function() {
		console.log("/api/"+period+"/"+cuid+"?" +
						"channel=" + channel + 
						"&sdate=" + sdate +
						"&edate=" + edate);
		var chart = AmCharts.makeChart("chartdiv", {
			"theme": "light",
			"type": "serial",
			"legend": {
				"horizontalGap": 10,
				"maxColumns": 3,
				"position": "top",
				"useGraphSettings": true,
				"markerSize": 10
			},"dataLoader": {
				"url": "/api/"+period+"/"+cuid+"?" +
						"channel=" + channel + 
						"&sdate=" + sdate +
						"&edate=" + edate,
				"format": "json"
			},
			"valueAxes": [{
				"id": "vaueAxis",
				"unit": "",
				"position": "left",
				"minimum" : 0,
				"title": "구매건수",
			},{
				"id": "ratioAxis",
				"position": "right",
				"minimum" : "0",
				"maximum" : "100",
				"unit": "%",
				"title": "비율"
			}],
			"graphs": [{
				"balloonText": "[[title]]: [[value]]",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"minimum" : 0,
				"title": "전체 구매 건수",
				"type": "column",
				"valueField": "purchaseCountTotal",
				"valueAxis": "valueAxis"
			},{
				"balloonText": "[[title]]: [[value]]",
				"fillAlphas": 0.9,
				"lineAlpha": 0.2,
				"title": "추천 경유 구매 건수",
				"type": "column",
				"clustered":false,
				"valueField": "purchaseCountReco",
				"valueAxis": "vaueAxis"
			},{
				"id": "graph3",
				"balloonText": "[[title]]: [[value]] %",
				"bullet": "round",
				"bulletBorderThickness": 3,
				"fillAlphas": 0,
				"lineAlpha": 1,
				"title": "전체 대비 추천 경유 구매 건수 비율",
				"valueField": "purchaseCountRate",
				"valueAxis": "ratioAxis"
			}],
				"chartScrollbar": {},
				"chartCursor": {
				"cursorAlpha": 0
			},
				"categoryField": "date",
				"categoryAxis": {
				"gridPosition": "start",
				"labelRotation": 45
			},
			"export": {
				"enabled": true
			}
		});
		$('#chartdiv').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
	}
	
	function dateToString(date) {
		if(date.getMonth()<=9||date.getDate()<=9){
			if(date.getMonth()<=9 && date.getDate()<=9){
				return date.getFullYear()+'-0'+date.getMonth()+'-0'+date.getDate();
			}else if(date.getMonth()<=9){
				return date.getFullYear()+'-0'+date.getMonth()+'-'+date.getDate();
			}else if(date.getDate()<=9){
				return date.getFullYear()+'-'+date.getMonth()+'-0'+date.getDate();
			}
		}
		return date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate();
	}
	
	function setCuid(newCuid) {
    	cuid = newCuid;
    }
	
	function setChannel(newChannel) {
		channel = newChannel;
	}
	
	function setPeriod(newPeriod) {
		period = newPeriod;
	}
	
	function setToDate(newToDate) {
		edate = newToDate;
	}
	
	function setFromDate(newFromDate) {
		sdate = newFromDate;
	}

	function setCurrency(newCurrency){
		currency = newCurrency;
	}
	
	return {
		
		init: function() {
			initChannel();
			initPeriod();
			initDayDate();
		},
		makeChart: function() {
			makeChart();
		},
		setCuid: function(newCuid) {
			setCuid(newCuid);
		},
		setChannel: function(newChannel) {
			setChannel(newChannel);
		},
		setPeriod: function(newPeriod) {
			setPeriod(newPeriod);
		},
		setToDate: function(newToDate) {
			setToDate(newToDate);
		},
		setFromDate: function(newFromDate) {
			setFromDate(newFromDate);
		},
		setCurrency: function(newCurrency){
			setCurrency(newCurrency);
		}
	};

}();

jQuery(document).ready(function() {    
	ChartsAmcharts.init();
});