'use strict';

function dateToString(date) {
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	if(month<=9||day<=9){
		if(month<=9 && day<=9){
			return year+'-0'+month+'-0'+day;
		}else if(month<=9){
			return year+'-0'+month+'-'+day;
		}else if(day<=9){
			return year+'-'+month+'-0'+day;
		}
	}
	return year+'-'+month+'-'+day;
}

angular.module('rbAdminApp')
    .controller('MainController', function ($scope, $http, Principal) {
    	Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
    		var tmp = new Date();
    		var cuid = account.cuid;
    		var sdate = dateToString(new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate()-2));
    		var edate = dateToString(new Date(tmp.getFullYear(), tmp.getMonth(), tmp.getDate()));

            $http({
            	method: 'GET',
            	url: "/api/dailyReports/"+cuid+"?" +
				"channel=AL" + 
				"&sdate=" + sdate +
				"&edate=" + edate
            }).then(function successCallback(response) {
            	$scope.reco = response.data[response.data.length - 1];
            	$scope.visitItemClickAvgResult = (Number($scope.reco.visitItemClickAvgResult) - 100).toFixed(2);
            	$scope.customerTransactionAvgResult = (Number($scope.reco.customerTransactionAvgResult) - 100).toFixed(2);
            	$scope.purchaseItemCountAvgResult = (Number($scope.reco.purchaseItemCountAvgResult) - 100).toFixed(2);
            	$scope.sessionPurchaseRevenueAvgResult = (Number($scope.reco.sessionPurchaseRevenueAvgResult) - 100).toFixed(2);
            	$scope.purchaseConversionRateResult = (Number($scope.reco.purchaseConversionRateResult) - 100).toFixed(2);
            }, function errorCallback(responese) {
            	
            });
            $('.tooltips').tooltip();            
        });
    });
