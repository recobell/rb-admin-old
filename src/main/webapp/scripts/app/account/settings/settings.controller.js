'use strict';

angular.module('rbAdminApp')
    .controller('SettingsController', function ($scope, Principal, Auth, Language, $translate) {
        $scope.infosuccess = null;
        $scope.infoerror = null;
        $scope.pwdsuccess = null;
        $scope.pwderror = null;
        $scope.pwddoNotMatch = null;
        
        Principal.identity(true).then(function(account) {
            $scope.settingsAccount = account;
        });

        $scope.accountSave = function () {
            Auth.updateAccount($scope.settingsAccount).then(function() {
                $scope.infoerror = null;
                $scope.infosuccess = 'OK';
                Principal.identity().then(function(account) {
                    $scope.settingsAccount = account;
                });
                Language.getCurrent().then(function(current) {
                    if ($scope.settingsAccount.langKey !== current) {
                        $translate.use($scope.settingsAccount.langKey);
                    }
                });
            }).catch(function() {
                $scope.infosuccess = null;
                $scope.infoerror = 'ERROR';
            });
        };
        
        $scope.passwordSave = function () {
        	if($scope.settingsAccount.newPassword == $scope.settingsAccount.newPasswordRetype){
        		Auth.changePassword($scope.settingsAccount.newPassword).then(function() {
        			$scope.pwdsuccess = true;
        			$scope.pwderror = null;
        			$scope.pwddoNotMatch = null;
        		});
        	}else{
        		$scope.pwdsuccess = null;
        		$scope.pwderror = null;
        		$scope.pwddoNotMatch = true;
        	}
        }
    });
