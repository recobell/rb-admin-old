'use strict';

angular.module('rbAdminApp')
    .controller('LoginController', function ($rootScope, $window, $scope, $state, $timeout, Auth, Principal) {
        $scope.user = {};
        $scope.errors = {};

        $scope.rememberMe = false;
        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        
        $scope.initLogin = function() {
        	Login.init();
        }
        
        $scope.login = function (event) {
            event.preventDefault();
            Auth.login({
                username: $scope.username,
                password: $scope.password,
                rememberMe: $scope.rememberMe
            }).then(function () {
                $scope.authenticationError = false;
                Principal.identity().then(function(account){
            		$window.sessionStorage.rbCuid = account.cuid;
            		$window.sessionStorage.rbCurrency = account.currency;
            	});
                if ($rootScope.previousStateName === 'register') {
                    $state.go('home');
                } else {
                    $rootScope.back();
                }
            }).catch(function () {
                $scope.authenticationError = true;
            });
        };
    });