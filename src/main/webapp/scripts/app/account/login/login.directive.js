'use strict';

angular.module('rbAdminApp')
	.directive('initLogin', function() {
		return {
			restrict : 'A',
			link : function ($scope) {
				$scope.initLogin();
			}
		}
	});