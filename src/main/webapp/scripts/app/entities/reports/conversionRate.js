'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('conversionRate', {
                parent: 'entity',
                url: '/reports/conversionRate',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.conversionRate.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/conversionRate.html',
                        controller: 'ConversionRateController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });