'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('numberOfClickPerVisit', {
                parent: 'entity',
                url: '/reports/numberOfClickPerVisit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.numberOfClickPerVisit.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/numberOfClickPerVisit.html',
                        controller: 'NumberOfClickPerVisitController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });