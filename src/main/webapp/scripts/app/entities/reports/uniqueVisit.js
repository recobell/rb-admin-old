'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('uniqueVisit', {
                parent: 'entity',
                url: '/reports/uniqueVisit',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.uniqueVisit.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/uniqueVisit.html',
                        controller: 'UniqueVisitController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });