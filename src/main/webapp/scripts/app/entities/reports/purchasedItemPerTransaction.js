'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('purchasedItemPerTransaction', {
                parent: 'entity',
                url: '/reports/purchasedItemPerTransaction',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.purchasedItemPerTransaction.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/purchasedItemPerTransaction.html',
                        controller: 'PurchasedItemPerTransactionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });