'use strict';

angular.module('rbAdminApp')
    .controller('UniqueVisitController', function ($scope, $state, $window, $modal, Principal) {
    	
    	$scope.reportType = "UniqueVisit";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });
