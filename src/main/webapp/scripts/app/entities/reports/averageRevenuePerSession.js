'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('averageRevenuePerSession', {
                parent: 'entity',
                url: '/reports/averageRevenuePerSession',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.averageRevenuePerSession.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/averageRevenuePerSession.html',
                        controller: 'AverageRevenuePerSessionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });