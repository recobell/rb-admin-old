'use strict';

angular.module('rbAdminApp')
    .controller('RevenueController', function ($scope, $state, $window, $modal, Principal) {
    	
    	$scope.reportType = "Revenue";
    	
    	$scope.chartLeftTitle = "매출 (" + Options.getCurrency() + ")"
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });