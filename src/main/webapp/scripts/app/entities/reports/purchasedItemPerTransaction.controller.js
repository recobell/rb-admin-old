'use strict';

angular.module('rbAdminApp')
    .controller('PurchasedItemPerTransactionController', function ($scope, $state, $window, $modal, Principal) {      

    	$scope.reportType = "PurchasedItemPerTransaction";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });