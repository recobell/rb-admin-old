'use strict';

angular.module('rbAdminApp')
    .controller('AverageRevenuePerSessionController', function ($scope, $state, $window, $modal, Principal) {      
 
    	$scope.reportType = "AverageRevenuePerSession";
    	
    	$scope.chartLeftTitle = "세션단위 평균 구매금액 (" + Options.getCurrency() + ")";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });