'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('revenue', {
                parent: 'entity',
                url: '/reports/revenue',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.revenue.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/revenue.html',
                        controller: 'RevenueController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });