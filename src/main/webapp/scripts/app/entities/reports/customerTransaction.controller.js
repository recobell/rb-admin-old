'use strict';

angular.module('rbAdminApp')
    .controller('CustomerTransactionController', function ($scope, $state, $window, $modal, Principal) {      
    	
    	$scope.reportType = "CustomerTransaction";
    	
    	$scope.chartLeftTitle = "객단가 (" + Options.getCurrency() + ")";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });