'use strict';

angular.module('rbAdminApp')
    .controller('ConversionRateController', function ($scope, $state, $window, $modal, Principal) {      

    	$scope.reportType = "ConversionRate";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });