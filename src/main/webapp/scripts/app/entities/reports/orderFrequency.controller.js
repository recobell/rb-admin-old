'use strict';

angular.module('rbAdminApp')
    .controller('OrderFrequencyController', function ($scope, $state, $window, $modal, Principal) {
    	
    	$scope.reportType = "OrderFrequency";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });