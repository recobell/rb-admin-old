'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('orderFrequency', {
                parent: 'entity',
                url: '/reports/orderFrequency',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.orderFrequency.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/orderFrequency.html',
                        controller: 'OrderFrequencyController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });