'use strict';

angular.module('rbAdminApp')
    .controller('ActiveUserController', function ($scope, $state, $window, $modal, Principal) {
    	
    	$scope.reportType = "ActiveUser";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });