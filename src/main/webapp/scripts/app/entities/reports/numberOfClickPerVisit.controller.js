'use strict';

angular.module('rbAdminApp')
    .controller('NumberOfClickPerVisitController', function ($scope, $state, $window, $modal, Principal) {      

    	$scope.reportType = "NumberOfClickPerVisit";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });