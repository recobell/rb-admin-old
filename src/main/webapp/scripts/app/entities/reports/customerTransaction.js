'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('customerTransaction', {
                parent: 'entity',
                url: '/reports/customerTransaction',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.report.customerTransaction.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reports/customerTransaction.html',
                        controller: 'CustomerTransactionController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    	$translatePartialLoader.addPart('sidebar');
                        $translatePartialLoader.addPart('reports');
                        return $translate.refresh();
                    }]
                }
            })
    });