'use strict';

angular.module('rbAdminApp')
    .controller('PageViewController', function ($scope, $state, $window, $modal, Principal) {      
    	
    	$scope.reportType = "PageView";
    	
    	$scope.chartLeftTitle = "";
    	
    	Principal.identity().then(function(account){
    		$scope.account = account;
    		$scope.isAuthenticated = Principal.isAuthenticated;
    	});
    	
    });