'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('reportDaily', {
                parent: 'entity',
                url: '/reportDailys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.reportDaily.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reportDaily/reportDailys.html',
                        controller: 'ReportDailyController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reportDaily');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('reportDaily.detail', {
                parent: 'entity',
                url: '/reportDaily/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.reportDaily.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reportDaily/reportDaily-detail.html',
                        controller: 'ReportDailyDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reportDaily');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ReportDaily', function($stateParams, ReportDaily) {
                        return ReportDaily.get({id : $stateParams.id});
                    }]
                }
            })
            .state('reportDaily.new', {
                parent: 'reportDaily',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportDaily/reportDaily-dialog.html',
                        controller: 'ReportDailyDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    cuid: null,
                                    date: null,
                                    channel: null,
                                    sessionVisitTotal: null,
                                    sessionVisitReco: null,
                                    sessionVisitRate: null,
                                    uniqueVisitTotal: null,
                                    uniqueVisitReco: null,
                                    uniqueVisitRate: null,
                                    pageViewTotal: null,
                                    pageViewReco: null,
                                    pageViewRate: null,
                                    purchaseCountTotal: null,
                                    purchaseCountReco: null,
                                    purchaseCountRate: null,
                                    revenueTotal: null,
                                    revenueReco: null,
                                    revenueRate: null,
                                    customerTransactionAvgTotal: null,
                                    customerTransactionAvgReco: null,
                                    customerTransactionAvgResult: null,
                                    purchaseItemCountAvgTotal: null,
                                    purchaseItemCountAvgReco: null,
                                    purchaseItemCountAvgResult: null,
                                    visitItemClickAvgTotal: null,
                                    visitItemClickAvgReco: null,
                                    visitItemClickAvgResult: null,
                                    purchaseConversionRateTotal: null,
                                    purchaseConversionRateReco: null,
                                    purchaseConversionRateResult: null,
                                    sessionPurchaseRevenueAvgTotal: null,
                                    sessionPurchaseRevenueAvgReco: null,
                                    sessionPurchaseRevenueAvgResult: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('reportDaily', null, { reload: true });
                    }, function() {
                        $state.go('reportDaily');
                    })
                }]
            })
            .state('reportDaily.edit', {
                parent: 'reportDaily',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportDaily/reportDaily-dialog.html',
                        controller: 'ReportDailyDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ReportDaily', function(ReportDaily) {
                                return ReportDaily.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reportDaily', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('reportDaily.delete', {
                parent: 'reportDaily',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportDaily/reportDaily-delete-dialog.html',
                        controller: 'ReportDailyDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ReportDaily', function(ReportDaily) {
                                return ReportDaily.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reportDaily', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
