'use strict';

angular.module('rbAdminApp').controller('ReportMonthlyDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ReportMonthly',
        function($scope, $stateParams, $modalInstance, entity, ReportMonthly) {

        $scope.reportMonthly = entity;
        $scope.load = function(id) {
            ReportMonthly.get({id : id}, function(result) {
                $scope.reportMonthly = result;
            });
        };

        var onSaveSuccess = function (result) {
            $scope.$emit('rbAdminApp:reportMonthlyUpdate', result);
            $modalInstance.close(result);
            $scope.isSaving = false;
        };

        var onSaveError = function (result) {
            $scope.isSaving = false;
        };

        $scope.save = function () {
            $scope.isSaving = true;
            if ($scope.reportMonthly.id != null) {
                ReportMonthly.update($scope.reportMonthly, onSaveSuccess, onSaveError);
            } else {
                ReportMonthly.save($scope.reportMonthly, onSaveSuccess, onSaveError);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
