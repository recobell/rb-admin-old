'use strict';

angular.module('rbAdminApp')
	.controller('ReportMonthlyDeleteController', function($scope, $modalInstance, entity, ReportMonthly) {

        $scope.reportMonthly = entity;
        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        $scope.confirmDelete = function (id) {
            ReportMonthly.delete({id: id},
                function () {
                    $modalInstance.close(true);
                });
        };

    });