'use strict';

angular.module('rbAdminApp')
    .controller('ReportMonthlyController', function ($scope, $state, $modal, ReportMonthly) {
      
        $scope.reportMonthlys = [];
        $scope.loadAll = function() {
            ReportMonthly.query(function(result) {
               $scope.reportMonthlys = result;
            });
        };
        $scope.loadAll();


        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.reportMonthly = {
                cuid: null,
                date: null,
                channel: null,
                sessionVisitTotal: null,
                sessionVisitReco: null,
                sessionVisitRate: null,
                uniqueVisitTotal: null,
                uniqueVisitReco: null,
                uniqueVisitRate: null,
                pageViewTotal: null,
                pageViewReco: null,
                pageViewRate: null,
                purchaseCountTotal: null,
                purchaseCountReco: null,
                purchaseCountRate: null,
                revenueTotal: null,
                revenueReco: null,
                revenueRate: null,
                customerTransactionAvgTotal: null,
                customerTransactionAvgReco: null,
                customerTransactionAvgResult: null,
                purchaseItemCountAvgTotal: null,
                purchaseItemCountAvgReco: null,
                purchaseItemCountAvgResult: null,
                visitItemClickAvgTotal: null,
                visitItemClickAvgReco: null,
                visitItemClickAvgResult: null,
                purchaseConversionRateTotal: null,
                purchaseConversionRateReco: null,
                purchaseConversionRateResult: null,
                sessionPurchaseRevenueAvgTotal: null,
                sessionPurchaseRevenueAvgReco: null,
                sessionPurchaseRevenueAvgResult: null,
                id: null
            };
        };
    });
