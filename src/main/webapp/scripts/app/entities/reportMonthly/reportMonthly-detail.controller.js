'use strict';

angular.module('rbAdminApp')
    .controller('ReportMonthlyDetailController', function ($scope, $rootScope, $stateParams, entity, ReportMonthly) {
        $scope.reportMonthly = entity;
        $scope.load = function (id) {
            ReportMonthly.get({id: id}, function(result) {
                $scope.reportMonthly = result;
            });
        };
        var unsubscribe = $rootScope.$on('rbAdminApp:reportMonthlyUpdate', function(event, result) {
            $scope.reportMonthly = result;
        });
        $scope.$on('$destroy', unsubscribe);

    });
