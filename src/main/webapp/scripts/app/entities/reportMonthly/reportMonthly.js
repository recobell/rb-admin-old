'use strict';

angular.module('rbAdminApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('reportMonthly', {
                parent: 'entity',
                url: '/reportMonthlys',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.reportMonthly.home.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reportMonthly/reportMonthlys.html',
                        controller: 'ReportMonthlyController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reportMonthly');
                        $translatePartialLoader.addPart('global');
                        return $translate.refresh();
                    }]
                }
            })
            .state('reportMonthly.detail', {
                parent: 'entity',
                url: '/reportMonthly/{id}',
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'rbAdminApp.reportMonthly.detail.title'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/reportMonthly/reportMonthly-detail.html',
                        controller: 'ReportMonthlyDetailController'
                    }
                },
                resolve: {
                    translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                        $translatePartialLoader.addPart('reportMonthly');
                        return $translate.refresh();
                    }],
                    entity: ['$stateParams', 'ReportMonthly', function($stateParams, ReportMonthly) {
                        return ReportMonthly.get({id : $stateParams.id});
                    }]
                }
            })
            .state('reportMonthly.new', {
                parent: 'reportMonthly',
                url: '/new',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportMonthly/reportMonthly-dialog.html',
                        controller: 'ReportMonthlyDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {
                                    cuid: null,
                                    date: null,
                                    channel: null,
                                    sessionVisitTotal: null,
                                    sessionVisitReco: null,
                                    sessionVisitRate: null,
                                    uniqueVisitTotal: null,
                                    uniqueVisitReco: null,
                                    uniqueVisitRate: null,
                                    pageViewTotal: null,
                                    pageViewReco: null,
                                    pageViewRate: null,
                                    purchaseCountTotal: null,
                                    purchaseCountReco: null,
                                    purchaseCountRate: null,
                                    revenueTotal: null,
                                    revenueReco: null,
                                    revenueRate: null,
                                    customerTransactionAvgTotal: null,
                                    customerTransactionAvgReco: null,
                                    customerTransactionAvgResult: null,
                                    purchaseItemCountAvgTotal: null,
                                    purchaseItemCountAvgReco: null,
                                    purchaseItemCountAvgResult: null,
                                    visitItemClickAvgTotal: null,
                                    visitItemClickAvgReco: null,
                                    visitItemClickAvgResult: null,
                                    purchaseConversionRateTotal: null,
                                    purchaseConversionRateReco: null,
                                    purchaseConversionRateResult: null,
                                    sessionPurchaseRevenueAvgTotal: null,
                                    sessionPurchaseRevenueAvgReco: null,
                                    sessionPurchaseRevenueAvgResult: null,
                                    id: null
                                };
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('reportMonthly', null, { reload: true });
                    }, function() {
                        $state.go('reportMonthly');
                    })
                }]
            })
            .state('reportMonthly.edit', {
                parent: 'reportMonthly',
                url: '/{id}/edit',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportMonthly/reportMonthly-dialog.html',
                        controller: 'ReportMonthlyDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ReportMonthly', function(ReportMonthly) {
                                return ReportMonthly.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reportMonthly', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('reportMonthly.delete', {
                parent: 'reportMonthly',
                url: '/{id}/delete',
                data: {
                    authorities: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/reportMonthly/reportMonthly-delete-dialog.html',
                        controller: 'ReportMonthlyDeleteController',
                        size: 'md',
                        resolve: {
                            entity: ['ReportMonthly', function(ReportMonthly) {
                                return ReportMonthly.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('reportMonthly', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
