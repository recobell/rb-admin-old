var Options = function () {
	
	var cuid;
	var currency;
	
	var channel;
	var period;
	var toDate;
	var fromDate;
	
	var init = function(cuid, currency) {
		setCuid(cuid);
		setCurrency(currency);
		setChannel("AL");
		setPeriod("dailyReports");
	}

	// set	
	function setCuid(newCuid) { cuid = newCuid; }
	function setCurrency(newCurrency) { currency = newCurrency; }
	function setChannel(newChannel) { channel = newChannel; }
	function setPeriod(newPeriod) { period = newPeriod; initDate(); }
	function setToDate(newToDate) { toDate = newToDate; }
	function setFromDate(newFromDate) { fromDate = newFromDate; }
	function setToDateString(newToDate) { toDate = new Date(newToDate); }
	function setFromDateString(newFromDate) { fromDate = new Date(newFromDate); }
	
	
	// get
	function getCuid() { return cuid; }
	function getCurrency() { return currency; }
	function getChannel() { return channel; }
	function getPeriod() { return period; }
	function getToDate() { return toDate; }
	function getFromDate(){ return fromDate; }
	function getToDateString() { return dateToString(toDate); }
	function getFromDateString(){ return dateToString(fromDate); }

	
	function initDate(){
		switch(period){
			case "dailyReports": initDailyDate(); break;
			case "monthlyReports": initMonthlyDate(); break;
			default:
		}
	}
	function initDailyDate() {
		var now = new Date();
		setToDate(new Date(now.getFullYear(), now.getMonth(), now.getDate()-1));
		setFromDate(new Date(now.getFullYear(), now.getMonth(), now.getDate()-14));
	}
	function initMonthlyDate(){
		var now = new Date();
		setToDate(new Date(now.getFullYear(), now.getMonth()-1, 1));
		setFromDate(new Date(now.getFullYear(), now.getMonth()-14, 1));
	}

	
	// test
	function printOptions(){
		console.log('cuid : ' + cuid);
		console.log('currency : ' + currency);
		console.log('channel : ' + channel);
		console.log('period : ' + period);
		console.log('toDate : ' + toDate);
		console.log('fromDate : ' + fromDate);
	}
	
	// date to string
	function dateToString(date){
		if(date.getMonth()<=8 || date.getDate()<=9){
			if(date.getMonth()<=8 && date.getDate()<=9){
				return date.getFullYear()+'-0'+(date.getMonth()+1)+'-0'+date.getDate();
			}else if(date.getMonth()<=8){
				return date.getFullYear()+'-0'+(date.getMonth()+1)+'-'+date.getDate();
			}else if(date.getDate()<=9){
				return date.getFullYear()+'-'+(date.getMonth()+1)+'-0'+date.getDate();
			}
		}
		return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
	}

	return {
		
		// init
		init:function(cuid, currency){
			init(cuid, currency);
		},
		
		// setter
		setCuid:function(newCuid) {setCuid(newCuid);},
		setCurrency:function(newCurrency) {setCurrency(newCurrency);},
		setChannel:function(newChannel) {setChannel(newChannel);},
		setPeriod:function(newPeriod) {setPeriod(newPeriod)},
		setToDate:function(newToDate) {setToDate(newToDate)},
		setFromDate:function(newFromDate) {setFromDate(newFromDate)},
		setToDateString:function(newToDate) {setToDateString(newToDate)},
		setFromDateString:function(newFromDate) {setFromDateString(newFromDate)},
		
		// getter
		getCuid:function() {return getCuid()},
		getCurrency:function() {return getCurrency()},
		getChannel:function() {return getChannel()},
		getPeriod:function() {return getPeriod()},
		getToDate:function() {return getToDate()},
		getFromDate:function() {return getFromDate()},
		getToDateString:function() {return getToDateString()},
		getFromDateString:function() {return getFromDateString()},
		
		// test
		printOptions: function() {printOptions();}
	}
}();