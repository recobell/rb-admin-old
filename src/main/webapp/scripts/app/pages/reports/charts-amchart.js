var ChartsAmchart = function() {
	
	var reportType;
	
	var reportLeftTitle;
	
	var reportsChartOptions;
	
	$.getJSON('scripts/app/pages/reports/charts-amchart.json', function(data){
		reportsChartOptions = data;
	});
	
	var makeChart = function() {
		var apiUrl = "/api/" + Options.getPeriod() + "/" + Options.getCuid() + "?" +
						"channel=" + Options.getChannel() +
						"&sdate=" + Options.getFromDateString() +
						"&edate=" + Options.getToDateString();
		
		reportsChartOptions[reportType]["dataLoader"]["url"] = apiUrl;
		
		switch(reportType){
			case "Revenue":
			case "CustomerTransaction":
			case "AverageRevenuePerSession":
				reportsChartOptions[reportType]["valueAxes"][0]["title"] = reportLeftTitle;
				reportsChartOptions[reportType]["graphs"][0]["balloonText"] = "[[title]]: " + Options.getCurrency() + " [[value]]";
				reportsChartOptions[reportType]["graphs"][1]["balloonText"] = "[[title]]: " + Options.getCurrency() + " [[value]]";
				break;
			default:
		}

		var chart = AmCharts.makeChart("chartdiv", reportsChartOptions[reportType]);
		$('#chartdiv').closest('.portlet').find('.fullscreen').click(function() {
            chart.invalidateSize();
        });
	}
	
	function setReportType(newReportType){
		reportType = newReportType;
	}
	
	function setReportLeftTitle(newReportLeftTitle){
		reportLeftTitle = newReportLeftTitle;
	}
	
	return {
		init: function(reportType, reportLeftTitle){
			setReportType(reportType);
			setReportLeftTitle(reportLeftTitle);
		},
		makeChart: function() {
			makeChart();
		}
	}
}();