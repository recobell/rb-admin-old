var ReportsOptions = function() {
	
	function initDate(){
		var from, to;
		switch(Options.getPeriod()){
			case "dailyReports": from = $('#date [name=from]');	to = $('#date [name=to]'); break;
			case "monthlyReports": from = $('#month [name=from]'); to = $('#month [name=to]'); break;
			default:
		}
		from.datepicker('setDate', Options.getFromDate());
		to.datepicker('setDate', Options.getToDate());
	}
	
    var selectChannel = function () {
    	$('#channel').on('click','.btn-group .btn',function(){
    		var tmp = $(this).context;
			Options.setChannel(tmp.name);
			makeReport();
		});
    }
    
    var selectPeriod = function () {
    	$('#period').on('click','.btn-group .btn', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'day': Options.setPeriod("dailyReports"); break;
	    		case 'month': Options.setPeriod("monthlyReports"); break;
	    		default:
    		}
    		initCalendar();    		
    		initDate();
    		changeDatePickerDisplay();
    		selectCalendar();
    		makeReport();
    	});
    }
    
    function initCalendar() {
    	$('#date').off('changeDate','.form-control');
		$('#month').off('changeDate','.form-control');
    }
    
    function selectCalendar() {
    	$('#date').on('changeDate','.form-control', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'to' : Options.setToDateString(tmp.value);break;
	    		case 'from' : Options.setFromDateString(tmp.value); break;
	    		default:
    		}
    		makeReport();
    	});
    	
    	$('#month').on('changeDate','.form-control', function(){
    		var tmp = $(this).context;
    		switch(tmp.name){
	    		case 'to' : Options.setToDateString(tmp.value + '-01'); break;
	    		case 'from' : Options.setFromDateString(tmp.value + '-01'); break;
	    		default:
    		}
    		makeReport();
    	});
    }
    
    function changeDatePickerDisplay(){
    	switch(Options.getPeriod()){
	    	case "dailyReports":
	    		$("#date").css('display','block');
    			$("#month").css('display','none');
	    		break;
	    	case "monthlyReports":
	    		$("#date").css('display','none');
    			$("#month").css('display','block');
	    		break;
	    	default:
    	}
    }
    
    function setChannel(channel){ Options.setChannel(channel); }
    function setPeriod(period){ Options.setPeriod(period); }
    
    function makeReport(){
    	ChartsAmchart.makeChart();
    	TablesDatatable.makeTable();
    }
    
    return {
        //main function to initiate the module
        init: function () {
        	initDate();
        	selectChannel();
        	selectPeriod();
        	selectCalendar();
        	changeDatePickerDisplay();
        	makeReport();
        }
    };

}();
