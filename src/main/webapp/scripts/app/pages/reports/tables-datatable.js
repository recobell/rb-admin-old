var TablesDatatable = function () {
	
	var reportType;
	
	var reportsTableOptions;
	
	$.getJSON('scripts/app/pages/reports/tables-datatable.json', function(data){
		reportsTableOptions = data;
	});
	
	var makeTable = function() {
		var apiUrl = "/api/" + Options.getPeriod() + "/" + Options.getCuid() + "?" +
						"channel=" + Options.getChannel() +
						"&sdate=" + Options.getFromDateString() +
						"&edate=" + Options.getToDateString();
		
		reportsTableOptions[reportType]["ajax"]["url"] = apiUrl;
		reportsTableOptions[reportType]["columns"][1]["mRender"] = function( data, type, full ) { return formatNumber(data); } 
		reportsTableOptions[reportType]["columns"][2]["mRender"] = function( data, type, full ) { return formatNumber(data); }
		reportsTableOptions[reportType]["columns"][3]["mRender"] = function( data, type, full ) { return data + '%'; }
		
		var table = $('#tablediv').dataTable(reportsTableOptions[reportType]);
	}
	
	function formatNumber(n) {
		return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	
	function setReportType(newReportType){
		reportType = newReportType;
	}
    
    return {
    	
    	init: function(reportType){
    		setReportType(reportType);
    	},
    	makeTable: function() {
			makeTable();
		}
    	
    }
}();
