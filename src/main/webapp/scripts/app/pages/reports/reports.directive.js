'use strict';

angular.module('rbAdminApp')
	.directive('setReport', function() {
		return {
			restrict : 'A',
			link : function ($scope) {
				ComponentsDateTimePickers.init();
				
				TablesDatatable.init($scope.reportType);
				ChartsAmchart.init($scope.reportType, $scope.chartLeftTitle);
			
				ReportsOptions.init();
			}
		}
	});