 'use strict';

angular.module('rbAdminApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-rbAdminApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-rbAdminApp-params')});
                }
                return response;
            }
        };
    });
