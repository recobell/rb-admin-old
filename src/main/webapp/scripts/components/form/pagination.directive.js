/* globals $ */
'use strict';

angular.module('rbAdminApp')
    .directive('rbAdminAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
