/* globals $ */
'use strict';

angular.module('rbAdminApp')
    .directive('rbAdminAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
