'use strict';

angular.module('rbAdminApp')
    .controller('NavbarController', function ($scope, $location, $window, $state, Auth, Principal, ENV) {
    	
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';
        
        $scope.initNavbar = function(){
        	Layout.initHeader();
        }
        
        $scope.isAuthenticated = function() {
        	//console.log('navbar : ' + Principal.isAuthenticated());
        	return Principal.isAuthenticated();
        };
        
        $scope.setService = function() {
        	Principal.identity().then(function(account){
        		$scope.account = account;
        		Options.init(account.cuid, account.currency);
        	});
        };

        $scope.logout = function () {
            Auth.logout();
            $state.go('login');
        };
       
    });