'use strict';

angular.module('rbAdminApp')
	.directive('initSidebar', function(){
		return {
			restrict : 'A',
			link : function ($scope) {
				$scope.initSidebar();
			}
		}
	});
