'use strict';

angular.module('rbAdminApp')
    .controller('SidebarController', function ($scope, $location, $state, Auth, Principal, ENV) {
    	
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';
        
        $scope.initSidebar = function(){
        	Layout.initSidebar();
        }
        
        $scope.isAuthenticated = function() {
        	return Principal.isAuthenticated();
        }
             
    });