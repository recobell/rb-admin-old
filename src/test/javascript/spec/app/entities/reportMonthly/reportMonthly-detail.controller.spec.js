'use strict';

describe('ReportMonthly Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockReportMonthly;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockReportMonthly = jasmine.createSpy('MockReportMonthly');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'ReportMonthly': MockReportMonthly
        };
        createController = function() {
            $injector.get('$controller')("ReportMonthlyDetailController", locals);
        };
    }));



});
