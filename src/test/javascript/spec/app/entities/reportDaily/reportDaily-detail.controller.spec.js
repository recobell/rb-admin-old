'use strict';

describe('ReportDaily Detail Controller', function() {
    var $scope, $rootScope;
    var MockEntity, MockReportDaily;
    var createController;

    beforeEach(inject(function($injector) {
        $rootScope = $injector.get('$rootScope');
        $scope = $rootScope.$new();
        MockEntity = jasmine.createSpy('MockEntity');
        MockReportDaily = jasmine.createSpy('MockReportDaily');
        

        var locals = {
            '$scope': $scope,
            '$rootScope': $rootScope,
            'entity': MockEntity ,
            'ReportDaily': MockReportDaily
        };
        createController = function() {
            $injector.get('$controller')("ReportDailyDetailController", locals);
        };
    }));



});
